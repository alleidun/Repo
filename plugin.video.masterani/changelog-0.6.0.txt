[0.6.0] - 2016-03-08
- Added search function
- Cleaned up the code
- Fixed a few bugs

[0.5.2] - 2016-02-08
- Accidentally deleted important files so addon did not work !
- Awesome me :P 

[0.5.1] - 2016-02-08
- Modified the sound of one of my farts
- Removed unused files
- Cleaned up a bit of code

[0.5.0] - 2016-29-07
- Added select dialog for Anime
- Messed up a lot of code
- Sang a song

[0.2.1] - 2016-23-07
- Updated addon to support new api structure from website
- Added Support for Bakavideo mirror

[0.2.0] - 2016-10-07
- Added ContextMenu Mark / Unmark as watched for show and episode
